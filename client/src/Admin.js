import React from 'react';
import {Link} from "@reach/router";
import axios  from 'axios';
import mkulimalenselogo from './mkulimalenselogo.png';
import Table from "./Table";
class Admin extends React.Component{
constructor(props){
    super(props);
    this.state={
        tableLoading: false,
        tableError: false,
        mkulimalenseadmindata:[]
    }
}
componentDidMount(){
    this.fetchMkulimalensedata()
}

fetchMkulimalensedata(){
    this.setState({tableLoading:true,tableError:false});
    axios.get("api/mkulimalense") 
    .then(response=>{
        this.setState({tableLoading:false,tableError:false,mkulimalenseadmindata:response.data})
    }) 
    .catch(error=>{
        this.setState({tableLoading:false,tableError:true,mkulimalenseadmindata:[]})
    })
}


    render(){
        const{mkulimalenseadmindata}=this.state

    return (
        <div>
            <h1>Data entry pane</h1>
        
            <img src={mkulimalenselogo} alt="mkulimalense logo"/>
            <p>this page is intended for use by authorised personale with proper clearence.this page contains sensitive informationand permissions pertaining to this page </p>

            <div>
    <div classname="entry">
    <label>seedoption1     </label><input type ="text" name ="seedoption1"></input><br></br>
    <label>seedoption2     </label><input type ="text" name ="seedoption2 "></input><br></br>
    <label>county          </label><input type ="text" name ="county"></input><br></br>
    <label>subcounty       </label><input type ="text" name ="subcounty"></input><br></br>
    <label>location        </label><input type ="text" name ="location"></input><br></br>
    <label>sublocation     </label><input type ="text" name ="sublocation"></input><br></br>
    
    
    <button className="form-submit" type="submit"> Submit </button>
    <button className="form-reset" type="reset"> Reset </button>
    </div>
 

</div>
<Table data={mkulimalenseadmindata}/>

<div classname="logout"> <h2>ADMIN LOGOUT</h2>
            <Link to="/">
            <button>LOGOUT</button>
            </Link>
            <label>generate log.txt</label><input></input>
            <button>download</button>
            </div>
           

 </div>
        
        
        
    )}
}

export default Admin;