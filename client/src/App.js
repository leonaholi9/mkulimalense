import React from 'react';
import Home from './Home';
import Mkulima from "./Mkulima";
import Admin from "./Admin";
import {Router} from "@reach/router";
import './App.css';

function App(){

return <div>
<Router>
<Home path="/"/>
<Mkulima path="/Mkulima"/>
<Admin path="/Admin"/>

</Router>
</div>
    }
export default App;