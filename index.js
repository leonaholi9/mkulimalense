require("dotenv").config();
const express = require("express");
const mysql = require("mysql");
const app = express();
const pool = mysql.createPool({
    host: process.env.DB_host,
    port: process.env.DB_port,
    user: process.env.DB_user,
    password: process.env.DB_password,
    database: process.env.DB_name
});

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
// RETRIEVE ENTRIES FROM LEON http://localhost:9000/api/mkulimalense
app.get("/api/mkulimalense", (req, res) => {
    pool.query("SELECT id,seed_option_2,seed_option_1,county,subcounty,location,sublocation from leon", (error, rows) => {
        if (error) {
            return res.status(500).json({ error });

        }

        res.json(rows);
    });
});
///RETRIEVE ENTRIES FROM LEON WHERE ID  http://localhost:9000/api/mkulimalense/13
app.get("/api/mkulimalense/:id", (req, res) => {
    pool.query("SELECT * FROM leon WHERE id = ?", [req.params.id],(error, rows) => {
        if (error) {
            return res.status(500).json({ error });

        }

        res.json(rows);
    });
});
//PUT ENTRIES IN MKULIMA TABLE  http://localhost:9000/api/mkulimalense
app.post("/api/mkulimalense", (req, res) => {
         const {county,subcounty,location,sublocation,pnone_no}= req.body;
    
         if (!county || !subcounty|| !location ||!sublocation ||!pnone_no) {
             return res.status(400).json({ error: "Invalid payload" });
         }
    
         pool.query(
             "INSERT INTO mkulima (county,subcounty,location,sublocation,pnone_no) VALUES(?,?,?,?,?)",
             [county,subcounty,location,sublocation,pnone_no],
             (error, results) => {
                 if (error) {
                     return res.status(500).json({ error });
                 }
    
                 res.json(results.insertId);
             }
         );
    });
// PUT ENTRIES IN LEON TABLE http://localhost:9000/api/leon
    app.post("/api/leon", (req, res) => {
        const {seed_option_2,seed_option_1,county,subcounty,location,sublocation}= req.body;
   
        if (!seed_option_2||!seed_option_1||!county || !subcounty|| !location ||!sublocation) {
            return res.status(400).json({ error: "Invalid payload" });
        }
   
        pool.query(
            "INSERT INTO leon (seed_option_2,seed_option_1,county,subcounty,location,sublocation) VALUES(?,?,?,?,?,?)",
            [seed_option_2,seed_option_1,county,subcounty,location,sublocation],
            (error, results) => {
                if (error) {
                    return res.status(500).json({ error });
                }
   
                res.json(results.insertId);
            }
        );
   });
//UPDATE ENTRIES IN LEON TABLE  http://localhost:9000/api/leon/8 
app.put("/api/leon/:id", (req, res) => {
         const {seed_option_2,seed_option_1,county,subcounty,location,sublocation} = req.body;
    
         if (!seed_option_2||!seed_option_1||!county || !subcounty|| !location ||!sublocation) {
             return res.status(400).json({ error: "Invalid payload" });
        }
    
         pool.query(
             "UPDATE leon SET seed_option_2=?,seed_option_1=?,county=?,subcounty=?,location=?,sublocation=? WHERE id=?",
             /*"Update leon SET (seed_option,seed_option,county,subcounty,location,sublocation)WHERE id= ?VALUES (?,?,?,?,?,?)",*/
             [seed_option_2,seed_option_1,county,subcounty,location,sublocation,req.params.id],
             (error, results) => {
                 if (error) {
                     return res.status(500).json({ error });
                 }
    
                 res.json(results.changedRows);
             }
     );
});
//DELETE ENTRIES IN LEON TABLE    http://localhost:9000/api/leon/1
app.delete("/api/leon/:id", (req, res) => {
    pool.query(
             "DELETE FROM leon WHERE id = ?",
             [req.params.id],
             (error, results) => {
                 if (error) {
                     return res.status(500).json({ error });
                 }
    
                 res.json(results.affectedRows);
             }
         );
     });


     

app.listen(9000, () => { console.log("App listening on port 9000") });
//happy coding